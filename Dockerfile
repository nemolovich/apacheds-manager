from nemolovich/php7_server:v1.0.1
LABEL maitainer "nemolovich<nemolovich@hotmail.fr>"

ENV ROOT_FOLDER "/var/www/html"

STOPSIGNAL SIGTERM

COPY ./board ${ROOT_FOLDER}

RUN chown -R www-data: ${ROOT_FOLDER}/
