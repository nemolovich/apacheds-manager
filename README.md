# ApacheDS Manager

Deploy a site manager for OpenLDAP Apache Directory Studio.

## With image

Docker images can be found here: https://hub.docker.com/repository/docker/nemolovich/apacheds-manager/tags?page=1&ordering=last_updated

## With standard docker

### Build

Build the container with:

```
docker build -t apacheds-manager ./
```

### Run

Run the server with:

```
docker run -d --name apacheds-manager \
  -v /opt/volumes/ca.pem:/etc/ssl/certs/ca-certificates.crt:ro
  -v /opt/board-config/config.cfg:/var/www/html/config/config.cfg:Z \
  -v /opt/board-config/sitemap.json:/var/www/html/config/sitemap.json:Z \
  apacheds-manager
```

The volume mount on `/etc/ssl/certs/ca-certificates.crt` is to allow certificates for LDAPS connexions.

## Access to web site

You can now access to your PHP site on container IP on the mapped port
(default port 8080). The phpinfo() page is displayed by default.

eg: http://localhost:8080

The first login will redirect you to the configuration page to set the LDAP informations.
Then you have to create groups and users if there is not on the LDAP server.
