<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Add group"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>

    <div class = "container">
      <div class="wrapper">
      <form action="<?=SERVER_ROOT.$_SERVER['PHP_SELF']?>" method="post" name="addUser" class="form-signin">
        <h3 class="form-signin-heading">New Group creation</h3>
        <hr class="colorgraph"><br>

        <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
        <?php include $_SERVER['DOCUMENT_ROOT']."/admin/checkadmin.php"; ?>
        <?php
          if (isset($_POST['group_cn'])) {
            $inserted = addGroup($_POST['group_cn'], $_POST['description'], $_POST['admin'], $_POST['default'], $_POST['users']);
            if (!$inserted) {
              $message[] = "Cannot add new group";
            }
            $_SESSION['message'] = $message;
            echo "<meta http-equiv='refresh' content='0'>";
          }
        ?>

        <input type="text" class="form-control" name="group_cn" placeholder="Group name" required="" />
        <input type="text" class="form-control" name="description" placeholder="Group description" required="" />
        <div class="checkbox">
          <label>
          <input type="checkbox" class="form-control checkbox-inline" name="admin" placeholder="Admin group" />
          Define this group as an <b>Admin</b> group
          </label>
        </div>
        <div class="checkbox">
          <label>
          <input type="checkbox" class="form-control checkbox-inline" name="default" placeholder="Default group" />
          Define this group as <b>the default</b> group
          </label>
        </div>
        <select class="form-control" name="users[]" size="8" required="" multiple="multiple">
        <?php
          foreach (getUsers() as $user) {
            echo "<option value=\"".$user['uid'][0]."\"".($user['uid'][0]==$_SESSION['user_id']?'selected="selected"': '').">".$user['displayname'][0]."</option>";
          }
        ?>
        </select>

        <button class="btn btn-lg btn-primary btn-block" name="Submit" value="Add new Group" type="submit">Add new Group</button>
        <?php
          if (isset($_GET['from'])) {
            $cancelUrl = SERVER_ROOT.$_GET['from'];
          } else {
            $cancelUrl = SERVER_ROOT.'/admin/managegroups.php';
          }
        ?>
        <button class="btn btn-block btn-default" name="Cancel" value="Cancel" type="button" onclick="window.location='<?=$cancelUrl?>';return false;">Cancel</button>
      </form>
      </div>
    </div>


  </body>
</html>
