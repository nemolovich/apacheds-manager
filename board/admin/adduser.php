<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Add user"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>

    <div class = "container">
      <div class="wrapper">
      <form action="<?=SERVER_ROOT.$_SERVER['PHP_SELF']?>" method="post" name="addUser" class="form-signin">
        <h3 class="form-signin-heading">New User creation</h3>

        <hr class="colorgraph"><br>
        
        <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
        <?php include $_SERVER['DOCUMENT_ROOT']."/admin/checkadmin.php"; ?>
        <?php
          if (isset($_POST['user_uid'])) {
            $inserted = addUser($_POST['user_uid'], $_POST['group'], $_POST['firstname'], $_POST['name'], $_POST['mail']);
            if (!$inserted) {
              $message[] = "Cannot add new user";
            }
            $_SESSION['message'] = $message;
            echo "<meta http-equiv='refresh' content='0'>";
          }
        ?>
        
        <input type="text" class="form-control" name="user_uid" placeholder="User ID/Login" required="" />
        <input type="text" class="form-control" name="firstname" placeholder="Firstname" required="" />
        <input type="text" class="form-control" name="name" placeholder="Name" required="" />
        <input type="email" class="form-control" name="mail" placeholder="Email" required="" />
        <select class="form-control" name="group" <?=NO_GROUPS ? 'disabled' : 'required=""'?>>
        <?php
          foreach (getGroups() as $group) {
            echo "<option value=\"".$group['cn'][0]."\"".($group['businesscategory'][0]=="default"?"selected=\"\" ":" ").">".$group['description'][0]."</option>";
          }
        ?>
        </select>
        <button class="btn btn-lg btn-primary btn-block" name="Submit" value="Add new User" type="submit">Add new User</button>
        <?php
          if (isset($_GET['from'])) {
            $cancelUrl = SERVER_ROOT.$_GET['from'];
          } else {
            $cancelUrl = SERVER_ROOT.'/admin/manageusers.php';
          }
        ?>
        <button class="btn btn-block btn-default" name="Cancel" value="Cancel" type="button" onclick="window.location='<?=$cancelUrl?>';return false;">Cancel</button>
      </form>
      </div>
    </div>


  </body>
</html>
