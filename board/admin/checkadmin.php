<?php
  if (!is_admin($_SESSION['user_dn']) && !NO_GROUPS) {
    $message[] = "You have to be administrator to access to this page.";
    $_SESSION['message_type'] = "danger";
    $_SESSION['message'] = $message;
    echo "<meta http-equiv='refresh' content='0; url=".LOGIN_PAGE."' >";
    exit(1);
  }
?>
