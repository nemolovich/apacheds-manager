<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Remove group"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
    <script>
      function confirmRemove() {
        return confirm('Remove group ' + document.getElementById('group_cn').value + '?');
      }
    </script>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>

    <div class = "container">
      <div class="wrapper">
        <form action="<?=SERVER_ROOT.$_SERVER['PHP_SELF']?>" method="post" name="delUser" class="form-signin">
          <h3 class="form-signin-heading">Remove a Group entry</h3>
          <hr class="colorgraph"><br>

          <?php include $_SERVER['DOCUMENT_ROOT']."/admin/checkadmin.php"; ?>
          <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
          <?php
            if($_POST['group_cn']) {
              $del_cn = $_POST['group_cn'];
              $removed = removeGroup($del_cn);
                      
              if ($removed) {
                $_SESSION['message_type'] = "success";
              } else {
                $_SESSION['message_type'] = "danger";
                $message[] = "Group <strong>$del_cn</strong> cannot be removed";
              }
              $_SESSION['message'] = $message;
              echo "<meta http-equiv='refresh' content='0'>";
            }
          ?>

          <select class="form-control" id="group_cn" name="group_cn" required="">
            <?php
              foreach (getGroups() as $group_array) {
                echo "<option value=\"".$group_array['cn'][0]."\">".$group_array['cn'][0]."</option>";
              }
            ?>
          </select>

          <button class="btn btn-lg btn-danger btn-block" name="Submit" value="Remove Group" type="submit" onclick="return confirmRemove();">Remove Group</button>
          <?php
            if (isset($_GET['from'])) {
              $cancelUrl = SERVER_ROOT.$_GET['from'];
            } else {
              $cancelUrl = SERVER_ROOT.'/admin/managegroups.php';
            }
          ?>
          <button class="btn btn-block btn-default" name="Cancel" value="Cancel" type="button" onclick="window.location='<?=$cancelUrl?>';return false;">Cancel</button>

        </form>
      </div>
    </div>


  </body>
</html>
