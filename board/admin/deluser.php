<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Remove user"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
    <script>
      function confirmRemove() {
        return confirm('Remove user ' + document.getElementById('user_uid').value + '?');
      }
    </script>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>

    <div class = "container">
      <div class="wrapper">
        <form action="<?=SERVER_ROOT.$_SERVER['PHP_SELF']?>" method="post" name="delUser" class="form-signin">
          <h3 class="form-signin-heading">Remove an User entry</h3>
          <hr class="colorgraph"><br>

          <?php include $_SERVER['DOCUMENT_ROOT']."/admin/checkadmin.php"; ?>
          <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
          <?php
            if($_POST['user_id'] && $_POST['user_id'] != $_SESSION['user_id']) {
              $del_uid = $_POST['user_id'];

              $removed = removeUser($del_uid);
              if ($removed) {
                $_SESSION['message_type'] = "success";
              } else {
                $_SESSION['message_type'] = "danger";
                $message[] = "User <strong>$del_uid</strong> cannot be removed";
              }
              $_SESSION['message'] = $message;
              echo "<meta http-equiv='refresh' content='0'>";
            }
          ?>

          <select class="form-control" id="user_uid" name="user_id" required="">
            <?php
              foreach (getUsers() as $user_array) {
                if ($user_array['uid'][0] != $_SESSION['user_id']) {
                  echo "<option value=\"".$user_array['uid'][0]."\">".$user_array['displayname'][0]."</option>";
                }
              }
            ?>
          </select>

          <button class="btn btn-lg btn-danger btn-block" name="Submit" value="Remove User" type="submit" onclick="return confirmRemove();">Remove User</button>
          <?php
            if (isset($_GET['from'])) {
              $cancelUrl = SERVER_ROOT.$_GET['from'];
            } else {
              $cancelUrl = SERVER_ROOT.'/admin/manageusers.php';
            }
          ?>
          <button class="btn btn-block btn-default" name="Cancel" value="Cancel" type="button" onclick="window.location='<?=$cancelUrl?>';return false;">Cancel</button>

        </form>
      </div>
    </div>


  </body>
</html>
