<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Edit Group"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
    <!-- Custom JS script -->
    <script src="<?=SERVER_ROOT?>/js/form.js"></script>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/admin/checkadmin.php"; ?>
    <?php
      $group_cn = $_GET['cn'];
      $group_ldap = getGroup($group_cn);
    ?>
    <div class = "container">
      <div class="wrapper">
        <form action="<?=SERVER_ROOT.$_SERVER['PHP_SELF']."?cn=$group_cn"?>" method="post" id="updateGroup"
            name="updateGroup" class="form-signin" enctype="multipart/form-data">
          <h3>Edit group</h3>
          <h4>Group: <?=$group_cn?></h4>

          <hr class="colorgraph"><br>

          <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>

          <?php
            $admin_group = in_array($group_cn, ADMINS_GROUPS);
            $default_group = $group_cn == DEFAULT_GROUP;
            /* Check if form has been submit */
            if (isset($_POST['group_cn'])) {
              global $message;
              $udpated = updateGroup($_POST['group_cn'], $_POST['description'], $_POST['admin'], $_POST['default'] || $default_group, $_POST['users']);
              if ($udpated) {
                $_SESSION['message_type'] = "success";
              } else {
                $message[] = "Modification error";
                $_SESSION['message_type'] = "danger";
              }
              $_SESSION['message'] = $message;
              echo "<meta http-equiv='refresh' content='0'>";
            }
          ?>
          <input type="hidden" name="group_cn" value="<?=$group_cn?>" />
          <label for="description">Description: </label>
          <input type="text" class="form-control" id="description" name="description" placeholder="Group description" value="<?=$group_ldap['description'][0]?>" required="" autofocus="" />

          <div class="checkbox">
            <label>
            <input type="checkbox" class="form-control checkbox-inline" name="admin" placeholder="Admin group" <?=$admin_group ? 'checked="checked"' : ''?> />
            Define this group as an <b>Admin</b> group
            </label>
          </div>
          <div class="checkbox">
            <label>
            <input type="checkbox" class="form-control checkbox-inline" name="default" placeholder="Default group" <?=$default_group ? 'checked="checked" disabled="disabled" title="Set a new default to change this value"' : ''?> />
            Define this group as <b>the default</b> group
            </label>
          </div>
          <select class="form-control" name="users[]" size="8" required="" multiple="multiple">
          <?php
            foreach (getUsers() as $user) {
              echo "<option value=\"".$user['uid'][0]."\"".(in_array($user['user_dn'], $group_ldap['member'])?'selected="selected"':'').">".$user['displayname'][0]."</option>";
            }
          ?>
          </select>
          
          <span class="help-text required">= Required fields</span>
          <button class="btn btn-lg btn-primary btn-block" name="Submit" value="Save" type="submit">Save</button>
          <?php
            if (isset($_GET['from'])) {
              $cancelUrl = SERVER_ROOT.$_GET['from'];
            } else {
              $cancelUrl = SERVER_ROOT.'/user/groupslist.php';
            }
          ?>
          <button class="btn btn-block btn-default" name="Cancel" value="Cancel" type="button" onclick="window.location='<?=$cancelUrl?>';return false;">Cancel</button>
        </form>
        <div id="file-content" class="file-content">
        </div>
      </div>
    </div>
  </body>
</html>
