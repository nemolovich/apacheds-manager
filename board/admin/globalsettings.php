<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Edit Settings"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
    <!-- Custom JS script -->
    <script src="<?=SERVER_ROOT?>/js/form.js"></script>
    <script>
      function checkUrl() {
        if (<?=NEED_INIT ? 'false' : 'true'?> && document.getElementById('ldap_server').value !== '<?=LDAP_URL?>') {
          return confirm('Caution: Changing the LDAP URL may cause login problems. Are you sure you want to continue?');
        }
        return true;
      }
    </script>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>
    <?php !NEED_INIT && include $_SERVER['DOCUMENT_ROOT']."/admin/checkadmin.php"; ?>
    <div class = "container">
      <div class="wrapper">
        <form action="<?=SERVER_ROOT.$_SERVER['PHP_SELF']?>" method="post" id="updateSettings"
              name="updateSettings" class="form-large" enctype="multipart/form-data"
              onsubmit="return checkUrl();">
          <h3>Edit settings</h3>

          <hr class="colorgraph"><br>

          <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>

          <?php
            /* Check if form has been submit */
            if (isset($_POST['settings'])) {
              global $message;
              $udpated = updateSettings($_POST);
              if ($udpated) {
                $_SESSION['message_type'] = "success";
              } else {
                $message[] = "Modification error";
                $_SESSION['message_type'] = "danger";
              }
              $_SESSION['message'] = $message;
              echo "<meta http-equiv='refresh' content='0'>";
            }
          ?>
          <input type="hidden" name="settings" value="submit" />

          <h4><small><em>--- Application configuration ---</em></small></h4>

          <label for="app_name">Application Name: </label>
          <input type="text" class="form-control" id="app_name" name="app_name" placeholder="My App" value="<?=APP_NAME?>" required="" autofocus="" />
          <label for="server_url">Server URL: </label>
          <span class="help-popup label label-info">Info
            <div>Change this value if you use a reverse proxy for example</div>
          </span>
          <br/>
          <span class="help-text">Server base path URL including context path for proxies</span>
          <input type="text" class="form-control" id="server_url" name="server_url" pattern="^https?://.+$" placeholder="http://localhost:8080" value="<?=SERVER_ROOT?>" required="" />
          <label for="session_timeout">Session timeout: </label>
          <br/><span class="help-text">Timeout in seconds</span>
          <input type="number" class="form-control" id="session_timeout" name="session_timeout" placeholder="3600" value="<?=SESSION_TIMEOUT?>" required="" />
          <label for="chat_link">Chat link: </label>
          <span class="help-popup label label-info">Info
            <div>
              <ul>
                <li>The chat link is used in users list to open chat using the user mail.</li>
                <li>The pattern replace <code>{%mail%}</code> by the user email</code></li>
                <li>Example for user with mail <code>user.name@example.com</code>: <code>sip:{%mail%}</code> will be replace by <code>sip:user.name@example.com</code></li>
              </ul>
            </div>
          </span>
          <br/>
          <span class="help-text">Link must include <code>{%mail%}</code></span>
          <input type="text" class="form-control" id="chat_link" name="chat_link" pattern="^.*\{%mail%\}.*$" placeholder="https://teams.microsoft.com/l/chat/0/0?users={%mail%}" value="<?=CHAT_LINK?>" required="" />
          
          <br/>
          <h4><small><em>--- LDAP configuration ---</em></small></h4>
          
          <label for="ldap_server">LDAP Server URL: </label>
          <input type="text" class="form-control" id="ldap_server" name="ldap_server" pattern="^ldaps?://.+$" placeholder="ldap://example.com:10389" value="<?=LDAP_URL?>" required="" />
          <label for="base_dn">LDAP Base DN: </label>
          <input type="text" class="form-control" id="base_dn" name="base_dn" placeholder="dc=example,dc=com" value="<?=BASE_DN?>" required="" />
          <label for="ou_users">Users OU: </label>
          <input type="text" class="form-control" id="ou_users" name="ou_users" placeholder="ou=users" value="<?=OU_USERS?>" required="" />
          <label for="ou_groups">Groups OU: </label>
          <input type="text" class="form-control" id="ou_users" name="ou_groups" placeholder="ou=groups" value="<?=OU_GROUPS?>" required="" />

          <label for="managed_groups[]">Managed groups: </label>
          <br/><span class="help-text">Groups that can be managed in this application</span>
          <select class="form-control" name="managed_groups[]" size="4" <?=!NEED_INIT ? 'required=""' : 'disabled'?> multiple="multiple">
          <?php foreach (getGroups() as $group) : ?>
              <? $group_cn = $group['cn'][0]; var_dump(MANAGED_GROUPS)?>
              <option value="<?=$group_cn?>" <?=in_array($group_cn, MANAGED_GROUPS)?'selected="selected"':''?>><?=$group_cn?></option>
          <? endforeach; ?>
          </select>
          <label for="default_group">Default group: </label>
          <br/><span class="help-text">Default group in which to add the new users</span>
          <input type="text" class="form-control" id="default_group" name="default_group" placeholder="users" value="<?=DEFAULT_GROUP?>" <?=!NEED_INIT ? 'required=""' : 'disabled'?> />
          <label for="admins_groups[]">Admins groups: </label>
          <br/><span class="help-text">The users in these groups are allow to modify other users and groups</span>
          <select class="form-control" name="admins_groups[]" size="4" <?=!NEED_INIT ? 'required=""' : 'disabled'?> multiple="multiple">
          <?php foreach (getGroups() as $group) : ?>
              <? $group_cn = $group['cn'][0]; ?>
              <option value="<?=$group_cn?>" <?=in_array($group_cn, ADMINS_GROUPS)?'selected="selected"':''?>><?=$group_cn?></option>
          <? endforeach; ?>
          </select>
          
          <span class="help-text required">= Required fields</span>
          <button class="btn btn-lg btn-primary btn-block" name="Submit" value="Save" type="submit">Save</button>
          <?php
            if (isset($_GET['from'])) {
              $cancelUrl = SERVER_ROOT.$_GET['from'];
            } else {
              $cancelUrl = SERVER_ROOT.'/';
            }
          ?>
          <button class="btn btn-block btn-default" name="Cancel" value="Cancel" type="button" onclick="window.location='<?=$cancelUrl?>';return false;">Cancel</button>
        </form>
        <div id="file-content" class="file-content">
        </div>
      </div>
    </div>
  </body>
</html>
