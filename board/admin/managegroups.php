<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Manage groups"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>

    <div class = "container">
      <div class="wrapper">
        <div class="form-signin">
        
          <h3 class="form-signin-heading">Manage Groups</h3>

          <hr class="colorgraph"><br>
          <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
          <?php include $_SERVER['DOCUMENT_ROOT']."/admin/checkadmin.php"; ?>
          <?php if(is_admin($_SESSION['user_dn'])) : ?>
            <div>
              <a class="btn btn-lg btn-primary btn-block" href="<?=SERVER_ROOT?>/admin/addgroup.php" title="Add a new Group entry">Add Group</a>
              <a class="btn btn-default btn-block" href="<?=SERVER_ROOT?>/user/groupslist.php" title="Display all groups Group entry">Groups List</a>
              <a class="btn btn-danger btn-block" href="<?=SERVER_ROOT?>/admin/delgroup.php" title="Remove a Group entry">Delete Group</a>
            </div>
          <?php else : ?>
            <a class="top-icon home" href="<?=SERVER_ROOT?>/">Go back to home</a>
          <?php endif; ?>
        </form>	
      </div>
    </div>
  </body>
</html>
