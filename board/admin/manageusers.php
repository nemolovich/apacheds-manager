<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Manage users"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>

    <div class = "container">
      <div class="wrapper">
        <div class="form-signin">
        
          <h3 class="form-signin-heading">Manage Users</h3>

          <hr class="colorgraph"><br>
          <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
          <?php include $_SERVER['DOCUMENT_ROOT']."/admin/checkadmin.php"; ?>
          <?php if(is_admin($_SESSION['user_dn'])) : ?>
            <div>
              <a class="btn btn-lg btn-primary btn-block" href="<?=SERVER_ROOT?>/admin/adduser.php" title="Add a new User entry">Add User</a>
              <a class="btn btn-default btn-block" href="<?=SERVER_ROOT?>/user/userslist.php" title="Display all users User entry">Users List</a>
              <a class="btn btn-danger btn-block" href="<?=SERVER_ROOT?>/admin/deluser.php" title="Remove an User entry">Delete User</a>
            </div>
          <?php else : ?>
            <a class="top-icon home" href="<?=SERVER_ROOT?>/">Go back to home</a>
          <?php endif; ?>
        </form>	
      </div>
    </div>
  </body>
</html>
