<?php
if (isset($_SESSION['message'])) {
  $message = $_SESSION['message'];
  if (isset($_SESSION['message_type'])) {
    $msg_t = $_SESSION['message_type'];
  } else {
    $msg_t = "info";
  }
  echo '<div class="alert alert-'.$msg_t.'">';
  foreach ($message as $one) {
    echo "$one<br>";
  }
  unset($_SESSION['message']);
  unset($_SESSION['message_type']);
  unset($message);
  $_SESSION['message'] = $message;
  echo '</div>';
}
?>