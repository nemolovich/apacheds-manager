<?php
  session_start();
  define("CONFIG_FILE", "/var/www/html/config/config.cfg");
  $config_content = readConfigFile(CONFIG_FILE);
  $default_config_content = readConfigFile("/var/www/html/config/default_config.cfg");
  $config = array_merge($default_config_content, $config_content);

  define("SITEMAP_FILE", "/var/www/html/config/sitemap.json");
  $default_sitemap = readJsonFile("/var/www/html/config/default_sitemap.json");
  define("DEFAULT_SITEMAP", serialize($default_sitemap));
  define("CUSTOM_SITEMAP", serialize(readJsonFile(SITEMAP_FILE)));
  define("SESSION_TIMEOUT", $config['session_timeout']);
  define("NEED_INIT", !file_exists(CONFIG_FILE));

  function getServerUrl() {
    $forward_proto = $_SERVER['HTTP_X_FORWARDED_PROTO'];
    $forward_host = $_SERVER['HTTP_X_FORWARDED_HOST'];
    $forward_port = $_SERVER['HTTP_X_FORWARDED_PORT'];
    $forward_path = $_SERVER['HTTP_X_FORWARDED_PREFIX'];
    $proto = empty($_SERVER['HTTPS']) ? "http" : "https";
    $host = $_SERVER['SERVER_NAME'];
    $port = $_SERVER['SERVER_PORT'];
    $script_name = $_SERVER['SCRIPT_NAME '];
    $server_proto = $forward_proto ?? $proto;
    $server_host = $forward_host ?? $host;
    $server_port = $forward_port ?? $port;
    $port_str = ($server_proto === 'http' && $server_port === '80' || $server_proto === 'https' && $server_port === '443') ? '' : ":$server_port";
    $server_path = $forward_path ?? '' ;
    return "$server_proto://$server_host$port_str$server_path";
  }

  $destroyed = !isset($_SESSION['user']);
  if (time() - $_SESSION['CREATED'] > SESSION_TIMEOUT) {
    if (isset($_SESSION['user'])) {
      $destroyed = true;
      session_destroy();
      session_unset();
      session_start();
      session_regenerate_id(true);
    }
  }
  $_SESSION['CREATED'] = time();
  if (isset($_SESSION['message_type'])&&isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    $message_css = true;
  } else {
    $message = array();
    $message_css = false;
  }
  define("SERVER_URL", $config['server_url']);
  define("SERVER_ROOT", empty(SERVER_URL) ? getServerUrl() : SERVER_URL);
  define("APP_NAME", $config['app_name']);
  define("LDAP_URL", $config['ldap_server']);
  $base_dn = $config['base_dn'];
  define("BASE_DN", "$base_dn");
  define("OU_USERS", $config['ou_users']);
  define("DN_USERS", OU_USERS.",$base_dn");
  define("OU_GROUPS", $config['ou_groups']);
  define("DN_GROUPS", OU_GROUPS.",$base_dn");
  define("LOGIN_PAGE", SERVER_ROOT."/login.php");
  define("DEFAULT_USER_ICON", SERVER_ROOT."/img/user-icon.png");
  define("MANAGED_GROUPS", explode(",", $config['managed_groups']));
  define("ADMINS_GROUPS", explode(",", $config['admins_groups']));
  define("DEFAULT_GROUP", $config['default_group']);
  define("CHAT_LINK", $config['chat_link']);
  $global_config = $config;
  define("NO_GROUPS", empty(array_filter(MANAGED_GROUPS)) && empty(array_filter(ADMINS_GROUPS)) && empty(DEFAULT_GROUP));

  if (NEED_INIT && $_SERVER['PHP_SELF'] != "/admin/globalsettings.php") {
    echo "<meta http-equiv='refresh' content='0; url=".SERVER_ROOT."/admin/globalsettings.php' >";
  }

  /* Errors */
  define("E101", "Error E101 - Incorrect user or password");
  define("E104", "Error E103 - Password must contains at least 8 characters");
  define("E104", "Error E104 - Password must contains at least one number");
  define("E105", "Error E105 - Password must contains at least one letter");
  define("E106", "Error E106 - Password must contains at least on upper character");
  define("E107", "Error E107 - Password must contains at least one lower character");
  define("E108", "Error E108 - Password verification failed");
  define("E200", "Error E200 - Cannot connect to server");
  define("E201", "Error E201 - User already exists");
  define("E202", "Error E202 - User does not exist");
  define("E203", "Error E203 - Group already exists");
  define("E204", "Error E204 - Group does not exist");
  define("E205", "Error E205 - Group must have at least one user");
  define("E206", "Error E206 - You cannot remove the default group");
  define("E207", "Error E207 - You cannot remove the only admin group");

  function getChatLink($mail) {
    return preg_replace("/\\{%mail%\\}/", $mail, CHAT_LINK);
  }

  /**
   * @see #is_in_group(string, string)
   */
  function is_admin($user_dn) {
    foreach (ADMINS_GROUPS as $group) {
      if (is_in_group($user_dn, "cn=$group,".DN_GROUPS)) {
        return true;
      }
    }
    return NO_GROUPS;
  }

  /**
   * @see #is_in_group(string, string)
   */
  function is_user($user_dn) {
    foreach (MANAGED_GROUPS as $group) {
      if (is_in_group($user_dn, "cn=$group,".DN_GROUPS)) {
        return true;
      }
    }
    return NO_GROUPS;
  }

  /**
   * Check if an user DN is present as member in a known group.
   * 
   * @param $user_dn {string} : The user DN entry to check
   * @return boolean : <code>true</code> if the user is a member
   * of a group, <code>false</code> otherwise.
   */
  function is_in_managed_group($user_dn) {
    return is_admin($user_dn) || is_user($user_dn);
  }

  /**
   * Check if a group CN is present a known group.
   * 
   * @param $group_dn {string} : The group CN entry to check
   * @return boolean : <code>true</code> if the group is managed,
   * <code>false</code> otherwise.
   */
  function is_managed_group($group_cn) {
    foreach (MANAGED_GROUPS as $group) {
      if ($group_cn === $group) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check if an user DN is present as member in a group.
   * 
   * @param $user_dn {string} : The user DN entry to check
   * @param $group_dn {string} : The group in which search
   * @return boolean : <code>true</code> if the user is a member
   * of the group, <code>false</code> otherwise.
   */
  function is_in_group($user_dn, $group_dn) {
    error_reporting(0);
    if (!isset($user_dn)) {
      return false;
    }
    $con = get_connection();

    $group_search = ldap_search($con, $group_dn, "(member=$user_dn)");
    $entries = ldap_get_entries($con, $group_search);

    if ($entries == null || empty($entries) || $entries['count'] === 0) {
      ldap_close($con);
      return false;
    }

    ldap_close($con);
    return true;
  }

  /**
   *
   * @return LDAP_CONNECTION|false : The LDAP connection if succeed.
   */
  function get_connection() {
    $con = ldap_connect(LDAP_URL);
    ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);
    return $con;
  }

  /**
   *
   * @return boolean : <code>true</code> if the connection succeed,
   * <code>false</code> otherwise.
   */
  function connect($user, $password) {
    global $message;

    $_SESSION['message_type'] = "danger";
    error_reporting(0);
    $con = get_connection();

    /* bind anon and find user by uid */
    $user_search = ldap_search($con, DN_USERS, "(|(uid=$user)(mail=$user))");

    $user_get = ldap_get_entries($con, $user_search);
    $user_entry = ldap_first_entry($con, $user_search);
    $user_dn = ldap_get_dn($con, $user_entry);
    $user_id = $user_get[0]["uid"][0];

    if (preg_match("/^ldaps:/", LDAP_URL)) {
      ldap_set_option($con, LDAP_OPT_REFERRALS, 0);
      ldap_start_tls($con);
    }

    $bind = ldap_bind($con, $user_dn, $password);
    /* Start the testing */
    if ($bind === false) {
      $message[] = E101;
      ldap_close($con);
      return false;
    }

    if (!$user_get) {
      $message[] = E202;
      ldap_close($con);
      return false;
    }

    /* And Finally, Assign user */
    if (!isset($_SESSION['CREATED'])) {
      $_SESSION['CREATED'] = time();
    } else if (time() - $_SESSION['CREATED'] > SESSION_TIMEOUT / 2) {
      session_regenerate_id(true);
      $_SESSION['CREATED'] = time();
    }
    $user_name = $user_get[0]["displayname"][0];
    $_SESSION['user'] = $user_name;
    $_SESSION['user_id'] = $user_id;
    $user_img = $user_get[0]["jpegphoto"][0];
    $_SESSION['ldap_user'] = $user_get;
    $_SESSION['user_dn'] = $user_dn;
    $_SESSION['img'] = $user_img;
    $message[] = "You are now connected";
    $_SESSION['message_type'] = "success";
    $_SESSION['message'] = $message;

    ldap_close($con);
    return true;
  }

  /**
   * Update a user entry.
   *
   * @param $user_id {string} : The uid field
   * @param $firstName {string} : The cn field
   * @param $lastName {string} : The sn field
   * @param $mail {string} : The mail field
   * @param $phoneNumber {string} : The telephoneNumber field
   * @param $mobileNumber {string} : The mobile field
   * @param $jobTitle {string} : The title field
   * @param $imgContent {string} : The jpegPhoto field
   * @param $newPassword {string} : The userPassword field
   * @param $newPasswordCnf {string} : The password verification
   * @return boolean : <code>true</code> if the user attributes have
   * been updated, <code>false</code> otherwise.
   */
  function updateUser($user_id, $firstName, $lastName, $mail, $phoneNumber, $mobileNumber, $jobTitle, $imgContent, $newPassword, $newPasswordCnf) {
    global $message;

    $_SESSION['message_type'] = "danger";
    error_reporting(0);
    $con = get_connection();

    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $message[] = E200;
      ldap_close($con);
      return false;
    }

    if ($newPassword) {
      if (!preg_match("/.{8,}/",$newPassword)) {
        $message[] = E103;
        ldap_close($con);
        return false;
      }

      if (!preg_match("/[0-9]/",$newPassword)) {
        $message[] = E104;
        ldap_close($con);
        return false;
      }

      if (!preg_match("/[a-zA-Z]/",$newPassword)) {
        $message[] = E105;
        ldap_close($con);
        return false;
      }

      if (!preg_match("/[A-Z]/",$newPassword)) {
        $message[] = E106;
        ldap_close($con);
        return false;
      }

      if (!preg_match("/[a-z]/",$newPassword)) {
        $message[] = E107;
        ldap_close($con);
        return false;
      }
    }
    /* Test passwords */
    if ($newPassword != $newPasswordCnf ) {
      $message[] = E108;
      ldap_close($con);
      return false;
    }

    /* Get user to check if it exists */
    $user_search = ldap_search($con, DN_USERS, "(uid=$user_id)");
    $user_get = ldap_get_entries($con, $user_search);
    if (!$user_get) {
      $message[] = E202;
      ldap_close($con);
      return false;
    }

    $user_dn = "uid=$user_id,".DN_USERS;
    $lastNameUpper = strtoupper($lastName);

    /* Form checks */
    $entry = array();
    $entry_add = array();
    $entry_del = array();
    /* Required */
    $entry["cn"] = "$firstName";
    $entry["sn"] = "$lastNameUpper";

    /* Required but can be missing */
    if ($user_get[0]['displayname']) {
      $entry["displayName"] = "$firstName $lastNameUpper";
    } else {
      $entry_add["displayName"] = "$firstName $lastNameUpper";
    }
    if ($user_get[0]['mail']) {
      $entry["mail"] = "$mail";
    } else {
      $entry_add["mail"] = "$mail";
    }

    /* Optionnals */
    if($phoneNumber) {
      $temp_phone = trim(preg_replace('/(\d{2})/', '\\1 ', preg_replace('/\s+/', '', $phoneNumber)));
      if ($user_get[0]['telephonenumber']) {
        $entry["telephoneNumber"] = "$temp_phone";
      } else {
        $entry_add["telephoneNumber"] = "$temp_phone";
      }
    } else {
      if ($user_get[0]['telephonenumber']) {
        $entry_del['telephoneNumber'] = array();
      }
    }
    if($mobileNumber) {
      $temp_phone = trim(preg_replace('/(\d{2})/', '\\1 ', preg_replace('/\s+/', '', $mobileNumber)));
      if ($user_get[0]['mobile']) {
        $entry["mobile"] = "$temp_phone";
      } else {
        $entry_add["mobile"] = "$temp_phone";
      }
    } else {
      if ($user_get[0]['mobile']) {
        $entry_del['mobile'] = array();
      }
    }
    if($jobTitle) {
      if ($user_get[0]['title']) {
        $entry["title"] = "$jobTitle";
      } else {
        $entry_add["title"] = "$jobTitle";
      }
    } else {
      if ($user_get[0]['title']) {
        $entry_del['title'] = array();
      }
    }
    if($imgContent) {
      if ($user_get[0]['jpegphoto']) {
        $entry["jpegPhoto"] = "$imgContent";
      } else {
        $entry_add["jpegPhoto"] = "$imgContent";
      }
    } else {
      if ($user_get[0]['jpegphoto']) {
        $entry_del['jpegPhoto'] = array();
      }
    }

    /* Remove new user flag attribute if exists */
    if ($user_get[0]['initials']) {
      $entry_del['initials'] = array();
    }

    /* Encode password if exists */
    if ($newPassword) {
      $encoded_newPassword = "{SHA}" . base64_encode(pack("H*", sha1($newPassword)));
      $entry["userPassword"] = "$encoded_newPassword";
    }

    /* DEBUG [
    $message[] = "<span>XX: $firstName</span>";
    $message[] = "<span>XX: $lastNameUpper</span>";
    $message[] = "<span>XX: $firstName $lastNameUpper</span>";
    $message[] = "<span>XX: $mail</span>";
    $message[] = "<span>XX: $phoneNumber</span>";
    $message[] = "<span>XX: $mobileNumber</span>";
    $message[] = "<span>XX: $imgContent</span>";
    $message[] = "<span>XX: $jobTitle</span>";
    $message[] = "<span>XX: $encoded_newPassword</span>";
    /* DEBUG ] */


    /* DEBUG [ /* 
    foreach ($entry as $k => $v) {
      $message[] = "Modify :: $k = $v";
    }
    foreach ($entry_add as $k => $v) {
      $message[] = "Add :: $k = $v";
    }
    foreach ($entry_del as $k => $v) {
      $message[] = "Del :: $k = $v";
    }
    /* DEBUG ] */

    $updated = empty($entry) ? true : ldap_modify($con, $user_dn, $entry);
    $inserted = empty($entry_add) ? true : ldap_mod_add($con, $user_dn, $entry_add);
    $deleted = empty($entry_del) ? true : ldap_mod_del($con, $user_dn, $entry_del);

    /* Check modification result */
    if (!$updated || !$inserted || !$deleted) {
      $error = ldap_error($con);
      $errno = ldap_errno($con);
      $message[] = "User cannot be updated, please contact a technical manager";
      $message[] = "$errno - $error";
      ldap_close($con);
      return false;
    }
    $message[] = "The user <strong>$user_id</strong> has been updated";

    $_SESSION['message_type'] = "success";
    ldap_close($con);
    return true;
  }

  function updateMyProfile($user_id, $firstName, $lastName, $mail, $phoneNumber, $mobileNumber, $jobTitle, $imgContent, $newPassword, $newPasswordCnf) {
    global $message;

    $updated = updateUser($user_id, $firstName, $lastName, $mail, $phoneNumber, $mobileNumber, $jobTitle, $imgContent, $newPassword, $newPasswordCnf);
    if ($updated) {
      error_reporting(0);
      $con = get_connection();
      $user_search = ldap_search($con, DN_USERS, "(uid=$user_id)");
      $user_get = ldap_get_entries($con, $user_search);
      $_SESSION['ldap_user'] = $user_get;
      $_SESSION['user'] = $user_get[0]['displayname'][0];
      $user_img = $user_get[0]["jpegphoto"][0];
      $_SESSION['img'] = $user_img;
      ldap_close($con);
    }
    return $updated;
  }

  function addUser($user_id, $group, $firstName, $lastName, $mail) {
    global $message;

    $_SESSION['message_type'] = "danger";
    error_reporting(0);
    $con = get_connection();

    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $message[] = E200;
      ldap_close($con);
      return false;
    }

    /* Check if user exists */
    $user_search = ldap_search($con, DN_USERS, "(uid=$user_id)");
    $user_get = ldap_get_entries($con, $user_search);

    if ($user_get != null && !empty($user_get) && $user_get['count'] > 0) {
      $message[] = E201;
      ldap_close($con);
      return false;
    }

    /* Create dn */
    $lastNameUpper = strtoupper($lastName);
    $dn_newuser = "uid=$user_id,".DN_USERS;

    /* Create default password */
    $encoded_newPassword = "{SHA}".base64_encode(pack("H*", sha1('changeit')));

    /* Create user */
    $entry = array();
    $entry["objectClass"][0] = "top";
    $entry["objectClass"][1] = "person";
    $entry["objectClass"][2] = "organizationalPerson";
    $entry["objectClass"][3] = "inetOrgPerson";
    $entry["userPassword"] = "$encoded_newPassword";
    $entry["cn"] = "$firstName";
    $entry["sn"] = "$lastNameUpper";
    $entry["displayName"] = "$firstName $lastNameUpper";
    $entry["mail"] = "$mail";
    $entry["uid"] = "$user_id";
    $entry["initials"] = "new";

    /* Add user to users dn */
    if (ldap_add($con, $dn_newuser, $entry) === false) {
      $error = ldap_error($con);
      $errno = ldap_errno($con);
      $message[] = "User cannot be added, please contact a technical manager";
      $message[] = "$errno - $error";
      ldap_close($con);
      return false;
    }
    $message[] = "The user <strong>$user_id</strong> has been created";

    /* Add user to group */
    $dn_newgroup = "cn=$group,".DN_GROUPS;
    $entry_goup = array();
    $entry_goup["member"] = "$dn_newuser";

    if (ldap_mod_add($con, $dn_newgroup, $entry_goup) === false) {
      $error = ldap_error($con);
      $errno = ldap_errno($con);
      $message[] = "Cannot add user to group <strong>$group</strong>, please contact a technical manager";
      $message[] = "$errno - $error";
      ldap_close($con);
      return false;
    }
    $message[] = "New user <strong>$user_id</strong> has been added to group <strong>$group</strong><br/><strong>The user is active an ready to be used</strong>";

    $_SESSION['message_type'] = "success";
    ldap_close($con);
    return true;
  }

  function addGroup($group_cn, $description, $isAdmin, $isDefault, $users) {
    global $message;
    global $global_config;

    $_SESSION['message_type'] = "danger";
    error_reporting(0);

    if (count($users) < 1) {
      $message[] = E205;
      return false;
    }

    $con = get_connection();

    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $message[] = E200;
      ldap_close($con);
      return false;
    }

    /* Check if group exists */
    $group_search = ldap_search($con, DN_GROUPS, "(cn=$group_cn)");
    $group_get = ldap_get_entries($con, $group_search);
    if ($group_get != null && !empty($group_get) && $group_get['count'] > 0) {
      $message[] = E203;
      ldap_close($con);
      return false;
    }

    /* Create dn */
    $dn_newgroup = "cn=$group_cn,".DN_GROUPS;

    /* Create group */
    $entry = array();
    $entry["objectClass"][0] = "groupOfNames";
    $entry["objectClass"][1] = "top";
    $entry["cn"] = "$group_cn";
    $entry["description"] = "$description";

    /* Add members */
    for ( $i = 0; $i < count($users); $i++ ) {
      $entry["member"][$i] = "uid=$users[$i],".DN_USERS;
    }

    /* Add group to groups dn */
    if (ldap_add($con, $dn_newgroup, $entry) === false) {
      $error = ldap_error($con);
      $errno = ldap_errno($con);
      $message[] = "Group cannot be added, please contact a technical manager";
      $message[] = "$errno - $error";
      ldap_close($con);
      return false;
    }
    $message[] = "The group <strong>$group_cn</strong> has been created";
    $_SESSION['message_type'] = "success";

    if ($isAdmin) {
      $global_config['admins_groups'] = implode(',', array_merge(ADMINS_GROUPS, [$group_cn]));
    }
    if ($isDefault) {
      $entry = array();
      $entry["businessCategory"] = "default";
      
      if (ldap_mod_add($con, $dn_newgroup, $entry) === false) {
        $error = ldap_error($con);
        $errno = ldap_errno($con);
        $_SESSION['message_type'] = "danger";
        $message[] = "Group cannot be set as default, please contact a technical manager";
        $message[] = "$errno - $error";
        ldap_close($con);
        return false;
      }
      $message[] = "The group <strong>$group_cn</strong> has been set as default";
      $global_config['default_group'] = "$group_cn";
      if (!unsetDefaultGroup()) {
        $_SESSION['message_type'] = "warning";
        $message[] = "The previous default group still have businessCategory attribute";
      }
    }
    $global_config['managed_groups'] = implode(',', array_merge(MANAGED_GROUPS, [$group_cn]));
    if (!saveConfigFile()) {
      $_SESSION['message_type'] = "danger";
      $message[] = "The config file could not be saved";
    }
    ldap_close($con);
    return true;
  }

  function unsetDefaultGroup() {
    global $message;

    error_reporting(0);
    $con = get_connection();
    
    $group_dn = "cn=".DEFAULT_GROUP.",".DN_GROUPS;
    $entry = array();
    $entry['businessCategory'] = ["default"];
    $deleted = ldap_mod_del($con, $group_dn, $entry);
    ldap_close($con);
    return $deleted;
  }

  function updateGroup($group_cn, $description, $isAdmin, $isDefault, $users) {
    global $message;
    global $global_config;

    $_SESSION['message_type'] = "danger";
    /* Not default */
    if (!$isDefault && $group_cn == DEFAULT_GROUP) {
      $message[] = E206;
      return false;
    }
    /* Not admin anymore */
    if (!$isAdmin && count(ADMINS_GROUPS) < 2 && in_array($group_cn, ADMINS_GROUPS)) {
      $message[] = E207;
      return false;
    }
    error_reporting(0);
    $con = get_connection();

    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $message[] = E200;
      ldap_close($con);
      return false;
    }

    /* Get current user to check if it exists */
    $group_search = ldap_search($con, DN_GROUPS, "(cn=$group_cn)");
    $group_get = ldap_get_entries($con, $group_search)[0];
    if (!$group_get) {
      $message[] = E204;
      ldap_close($con);
      return false;
    }
    $group_dn = "cn=$group_cn,".DN_GROUPS;

    /* Form checks */
    $entry = array();
    $entry_add = array();
    $entry_del = array();

    /* Required */
    $entry["description"] = "$description";

    $curr_members = $group_get['member'];
    /* Add members */
    for ( $i = 0; $i < count($users); $i++ ) {
      $user_dn = "uid=$users[$i],".DN_USERS;
      if (!in_array($user_dn, $curr_members)) {
        if (!isset($entry_add["member"])) {
          $entry_add["member"] = array();
        }
        $entry_add["member"][] = $user_dn;
      }
    }
    /* Remove non-members */
    for ( $i = 0; $i < count($curr_members); $i++ ) {
      $user_uid = preg_replace('/^uid=(\w+),.+$/', '$1', $curr_members[$i]);
      if (empty($user_uid)) {
        continue;
      }
      if (!in_array($user_uid, $users)) {
        if (!isset($entry_del["member"])) {
          $entry_del["member"] = array();
        }
        $entry_del["member"][] = "uid=$user_uid,".DN_USERS;
      }
    }

    $updated = empty($entry) ? true : ldap_modify($con, $group_dn, $entry);
    $inserted = empty($entry_add) ? true : ldap_mod_add($con, $group_dn, $entry_add);
    $deleted = empty($entry_del) ? true : ldap_mod_del($con, $group_dn, $entry_del);

    /* Check modification result */
    if (!$updated || !$inserted || !$deleted) {
      $error = ldap_error($con);
      $errno = ldap_errno($con);
      $message[] = "Group cannot be updated, please contact a technical manager";
      $message[] = "$errno - $error";
      ldap_close($con);
      return false;
    }
    $message[] = "The group <strong>$group_cn</strong> has been updated";
    $_SESSION['message_type'] = "success";

    $configModified = false;

    /* New default */
    if ($isDefault && $group_cn != DEFAULT_GROUP) {
      $entry = array();
      $entry["businessCategory"] = "default";
      
      if (ldap_mod_add($con, $group_dn, $entry) === false) {
        $error = ldap_error($con);
        $errno = ldap_errno($con);
        $_SESSION['message_type'] = "danger";
        $message[] = "Group cannot be set as default, please contact a technical manager";
        $message[] = "$errno - $error";
        ldap_close($con);
        return false;
      }
      $message[] = "The group <strong>$group_cn</strong> has been set as default";
      $global_config['default_group'] = "$group_cn";
      $configModified = true;
      if (!unsetDefaultGroup()) {
        $_SESSION['message_type'] = "warning";
        $message[] = "The previous default group still have businessCategory attribute";
      }
    }

    /* New admin */
    if ($isAdmin && !in_array($group_cn, ADMINS_GROUPS)) {
      $global_config['admins_groups'] = implode(',', array_merge(ADMINS_GROUPS, [$group_cn]));
      $configModified = true;
    }

    /* Not admin anymore */
    if (!$isAdmin && in_array($group_cn, ADMINS_GROUPS)) {
      $global_config['admins_groups'] = implode(',', array_diff(ADMINS_GROUPS, [$group_cn]));
      $configModified = true;
    }

    if ($configModified) {
      if (!saveConfigFile()) {
        $_SESSION['message_type'] = "danger";
        $message[] = "The config file could not be saved";
      }
    }

    ldap_close($con);
    return true;
  }

  function removeUserFromGroup($user_uid, $group_cn) {
    global $message;

    $_SESSION['message_type'] = "danger";
    error_reporting(0);
    $con = get_connection();

    /* Get current user to check if it exists */
    $user_search = ldap_search($con, DN_USERS, "(uid=$user_uid)");
    $user_entry = ldap_first_entry($con, $user_search);
    if (!$user_entry) {
      $message[] = E202;
      ldap_close($con);
      return false;
    }

    if ($_SESSION['user_id'] === $user_uid) {
      $message[] = "You can not remove yourself! WTF!?!";
      ldap_close($con);
      return false;
    }

    $user_dn = "uid=$user_uid,".DN_USERS;
    $group_dn = "cn=$group_cn,".DN_GROUPS;

    $entry_del = array();
    $entry_del['member'] = $user_dn;

    $deleted = empty($entry_del) ? true : ldap_mod_del($con, $group_dn, $entry_del);

    /* Check modification result */
    if (!$deleted) {
      $error = ldap_error($con);
      $errno = ldap_errno($con);
      $message[] = "The user <strong>$user_uid</strong> cannot be removed from group <strong>$group_cn</strong>";
      $message[] = "$errno - $error";
      ldap_close($con);
      return false;
    }
    $message[] = "User <strong>$user_uid</strong> has been removed from group <strong>$group_cn</strong>";

    $_SESSION['message_type'] = "success";
    ldap_close($con);
    return true;
  }

  function removeUser($user_id) {
    global $message;

    $_SESSION['message_type'] = "danger";
    error_reporting(0);

    if ($_SESSION['user_id'] === $user_uid) {
      $message[] = "You can not remove yourself! WTF!?!";
      return false;
    }

    $con = get_connection();
    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $message[] = E200;
      ldap_close($con);
      return false;
    }
    ldap_close($con);

    $removed = false;
    foreach (getUsersWithGroup() as $user) {
      $curr_uid = $user['uid'][0];
      if ($user_id === $curr_uid) {
        $user_groups = $user['groups_cn'];
        $user_dn = $user['user_dn'];
        $removed = true;
        foreach ($user_groups as $group_cn) {
          $removed = $removed && removeUserFromGroup($user_id, $group_cn);
        }
        break;
      }
    }

    $con = get_connection();
    /* Get user to check if it exists */
    $user_search = ldap_search($con, DN_USERS, "(uid=$user_id)");
    $user_entry = ldap_first_entry($con, $user_search);
    if (!$user_entry) {
      $message[] = E202;
      ldap_close($con);
      return false;
    }

    if (ldap_delete($con, $user_dn) === false) {
      $error = ldap_error($con);
      $errno = ldap_errno($con);
      $message[] = "Cannot remove user <strong>$user_id</strong>, please contact a technical manager";
      $message[] = "$errno - $error";
      ldap_close($con);
      return false;
    }
    $message[] = "User <strong>$user_id</strong> has been removed</strong>";

    ldap_close($con);
    return $removed;
  }

  function removeGroup($group_cn) {
    global $message;
    global $global_config;

    $_SESSION['message_type'] = "danger";
    error_reporting(0);

    if ($group_cn == DEFAULT_GROUP) {
      $message[] = E206;
      return false;
    }
    /* Not admin anymore */
    if (count(ADMINS_GROUPS) < 2 && in_array($group_cn, ADMINS_GROUPS)) {
      $message[] = E207;
      return false;
    }

    $con = get_connection();

    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $message[] = E200;
      ldap_close($con);
      return false;
    }

    /* Get current group to check if it exists */
    $group_search = ldap_search($con, DN_GROUPS, "(cn=$group_cn)");
    $group_entry = ldap_first_entry($con, $group_search);
    if (!$group_entry) {
      $message[] = E204;
      ldap_close($con);
      return false;
    }

    $group_dn = ldap_get_dn($con, $group_entry);
    if (ldap_delete($con, $group_dn) === false) {
      $error = ldap_error($con);
      $errno = ldap_errno($con);
      $message[] = "Cannot remove group <strong>$group_cn</strong>, please contact a technical manager";
      $message[] = "$errno - $error";
      ldap_close($con);
      return false;
    }
    $message[] = "Group <strong>$group_cn</strong> has been removed</strong>";

    if (in_array($group_cn, ADMINS_GROUPS)) {
      $global_config['admins_groups'] = implode(',', array_diff(ADMINS_GROUPS, [$group_cn]));
    }
    $global_config['managed_groups'] = implode(',', array_diff(MANAGED_GROUPS, [$group_cn]));
    if (!saveConfigFile()) {
      $_SESSION['message_type'] = "danger";
      $message[] = "The config file could not be saved";
    }

    ldap_close($con);
    return true;
  }

  function getUser($uid) {

    error_reporting(0);
    $con = get_connection();

    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $_SESSION['message_type'] = "danger";
      $message[] = E200;
      $_SESSION['message'] = $message;
      ldap_close($con);
      return null;
    }

    /* Check if user exists */
    $users_search = ldap_search($con, DN_USERS, "(uid=$uid)");
    ldap_sort($con, $users_search, 'uid');
    $user = ldap_get_entries($con, $users_search)[0];
    $user['user_dn'] = "uid=$uid,".DN_USERS;
    ldap_close($con);
    return $user;
  }

  function getUsers() {

    error_reporting(0);
    $con = get_connection();

    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $_SESSION['message_type'] = "danger";
      $message[] = E200;
      $_SESSION['message'] = $message;
      ldap_close($con);
      return null;
    }

    /* Check if user exists */
    $users_search = ldap_search($con, DN_USERS, "(uid=*)");
    ldap_sort($con, $users_search, 'uid');
    $users_get = ldap_get_entries($con, $users_search);

    $users = array();
    foreach ($users_get as $user) {
      $user_uid = $user['uid'][0];
      if (!empty($user_uid)) {
        $user['user_dn'] = "uid=$user_uid,".DN_USERS;
        $users[] = $user;
      }
    }
    ldap_close($con);
    return $users;
  }

  function getUsersWithGroup() {
    global $message;

    error_reporting(0);
    $con = get_connection();

    $users_get = getUsers();

    $users = array();
    foreach ($users_get as $user) {
      $user_uid = $user['uid'][0];
      $user_dn = null;
      if (!empty($user_uid)) {
        $user_search = ldap_search($con, DN_USERS, "(uid=$user_uid)");
        $user_entry = ldap_first_entry($con, $user_search);
        $user_dn = ldap_get_dn($con, $user_entry);

        $user_groups = array();

        $groups = getGroups();
        foreach ($groups as $group) {
          $cn = trim($group["cn"][0]);
          if (!empty($cn)) {
            $group_search = ldap_search($con, DN_GROUPS, "(cn=$cn)");
            $group_entry = ldap_first_entry($con, $group_search);
            $group_dn = ldap_get_dn($con, $group_entry);
            if (is_in_group($user_dn, $group_dn)) {
              $user_groups[] = $cn;
            }
          }
        }

        $user['user_dn'] = $user_dn;
        $user['groups_cn'] = $user_groups;
        $users[] = $user;

      }
    }

    ldap_close($con);
    return $users;
  }

  function getGroupsWithUsers() {
    global $message;

    error_reporting(0);
    $con = get_connection();

    $get_groups = getGroups();

    $groups = array();
    foreach ($get_groups as $group) {
      $group_cn = trim($group['cn'][0]);
      $group_dn = null;
      if (!empty($group_cn)) {
        $group_search = ldap_search($con, DN_GROUPS, "(cn=$group_cn)");
        $group_entry = ldap_first_entry($con, $group_search);
        $group_dn = ldap_get_dn($con, $group_entry);
        $group_members = ldap_get_entries($con, $group_search)[0]['member'];

        $group_users = array();

        for ( $i = 0; $i < $group_members['count']; $i++ ) {
          $user_dn = $group_members[$i];
          if (!empty($user_dn)) {
            $user_uid = str_replace("uid=", "", explode(",", $user_dn)[0]);
            $user_search = ldap_search($con, DN_USERS, "(uid=$user_uid)");
            $user_entry = ldap_get_entries($con, $user_search)[0];
            if ($user_entry != null && !empty($user_entry) || $user_entry['count'] === 0) {
              $user_name = $user_entry['displayname'][0];
              $user = array();
              $user['uid'] = $user_uid;
              $user['name'] = $user_name;
              $group_users[] = $user;
            }
          }
        }

        $group['group_dn'] = $group_dn;
        $group['users'] = $group_users;
        $groups[] = $group;

      }
    }

    ldap_close($con);
    return $groups;
  }

  function getGroup($cn) {

    error_reporting(0);
    $con = get_connection();

    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $_SESSION['message_type'] = "danger";
      $message[] = E200;
      $_SESSION['message'] = $message;
      ldap_close($con);
      return null;
    }

    /* Check if group exists */
    $groups_search = ldap_search($con, DN_GROUPS, "(cn=$cn)");
    ldap_sort($con, $groups_search, 'cn');
    $group = ldap_get_entries($con, $groups_search)[0];
    $group['grounsetup_dn'] = "cn=$cn,".DN_GROUPS;
    ldap_close($con);
    return $group;
  }

  function getGroups() {

    error_reporting(0);
    $con = get_connection();

    /* get current user to check connection */
    $curr_uid = $_SESSION['user_id'];
    $admin_search = ldap_search($con, DN_USERS, "(uid=$curr_uid)");
    $admin_get = ldap_get_entries($con, $admin_search);

    if (!$admin_get) {
      $_SESSION['message_type'] = "danger";
      $message[] = E200;
      $_SESSION['message'] = $message;
      ldap_close($con);
      return false;
    }

    /* Find groups */
    $groups_search_filter = "(objectClass=groupOfNames)";
    $groups_search = ldap_search($con, DN_GROUPS, $groups_search_filter);
    $groups_get = ldap_get_entries($con, $groups_search);

    $groups = array();
    foreach ($groups_get as $group) {
      $cn = trim($group["cn"][0]);
      if (!empty($cn)) {
        $groups[] = $group;
      }
    }
    ldap_close($con);
    return $groups;
  }

  function updateSettings($new_config) {
    global $message;
    global $global_config;

    error_reporting(0);
    $temp_config = array_merge($global_config);
    foreach ($global_config as $k => $v) {
      if (array_key_exists($k, $new_config) && !is_null($v)) {
        $new_v = $new_config[$k];
        if (is_array($new_v)) {
          $temp_config[$k] = implode(',', $new_v);
        } else {
          $temp_config[$k] = $new_v;
        }
      }
    }
    $global_config = $temp_config;
    if (NEED_INIT) {
      $global_config['managed_groups'] = '';
      $global_config['admins_groups'] = '';
      $global_config['default_group'] = '';
    }
    $_SESSION['message_type'] = "success";
    if (!saveConfigFile()) {
      $_SESSION['message_type'] = "danger";
      $message[] = "The config file could not be saved";
    }
    $message[] = "Settings updated";

    return true;
  }

  function readConfigFile($config_path) {
    $content = array();
    if (file_exists($config_path)) {
      $content = parse_ini_file($config_path, false);
    }
    return $content;
  }

  function readJsonFile($file_path) {
    $content = array();
    if (file_exists($file_path)) {
      $file_content = file_get_contents($file_path);
      $content = json_decode($file_content);
    }
    return $content;
  }

  function saveConfigFile() {
    global $global_config;

    return write_init_file(CONFIG_FILE, $global_config);
  }

  function write_init_file($fileName, $content) {
    $fileContent = '';
    foreach ($content as $k => $v) {
      if (is_numeric($v) or is_bool($v)) {
        $fileContent .= "$k=$v".PHP_EOL;
      } else {
        $fileContent .= "$k=\"$v\"".PHP_EOL;
      }
    }

    $succeed = true;
    if (!$handle = fopen($fileName, 'w')) { 
      $succeed = false; 
    }

    $succeed = $succeed && fwrite($handle, $fileContent.PHP_EOL);
    fclose($handle); 
    return $succeed;
  }

  function getFormImgContent($defaultContent) {
    /* Check if the image has been removed */
    if ($_POST['remove-img'] == "true" ) {
      $imgContent = null;
    } else {
      /* If there is a file in the avatar input then read content */
      if ($_FILES["avatar"] && $_FILES["avatar"]["name"]) {
        /* Temporary file path */
        $file_path = $_FILES["avatar"]["tmp_name"];
        /* Image MIME type */
        $image_mime = getimagesize($file_path)['mime'];
        /* Check format */
        if (preg_match("/^image\/(png|jpeg)$/", $image_mime)) {
          /* If the image is a PNG then convert to JPEG */
          if (preg_match("/^image\/png$/", $image_mime)) {
            /* New file path */
            $new_file = $file_path.".jpg";
            /* Create new Image */
            $image = imagecreatefrompng($file_path);
            /* Apply colors for JPEG */
            $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
            /* Make background white */
            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
            /* Bind background for alpha color */
            imagealphablending($bg, TRUE);
            // /* Copy PNG to JPEG image */
            imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
            /* Destruct image */
            imagedestroy($image);
            /* JPEG Quality: 0 = worst / smaller file, 100 = better / bigger file */
            $quality = 100;
            /* Apply image content on new file */
            imagejpeg($bg, $new_file, $quality);
            /* Destroy content */
            imagedestroy($bg);

            $file_path = $new_file;
          }
          /* Read file content */
          $file_content = file_get_contents($file_path, FILE_USE_INCLUDE_PATH);
          $imgContent = $file_content;
        } else {
          $message = "Invalid image type";
          $_SESSION['message_type'] = "warning";
          $imgContent = null;
        }
      } else {
        /* Get actual content if there is no file */
        $imgContent = $defaultContent;
      }
    }
    return $imgContent;
  }

?>
