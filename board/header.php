    <?php include $_SERVER['DOCUMENT_ROOT']."/functions.php" ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="<?=SERVER_ROOT?>/img/favicon.ico" />
    <link rel="stylesheet" href="<?=SERVER_ROOT?>/css/style.css" />
    <link rel="stylesheet" href="<?=SERVER_ROOT?>/css/top-icon.css" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?=SERVER_ROOT?>/css/bootstrap.3.6.6.min.css" media="screen"/>
    <!-- Optional theme -->
    <link rel="stylesheet" href="<?=SERVER_ROOT?>/css/bootstrap-theme.3.6.6.min.css" />
    <!-- jQuery library -->
    <script src="<?=SERVER_ROOT?>/js/jquery.1.12.4.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?=SERVER_ROOT?>/js/bootstrap.3.3.6.min.js"></script>
    <title><?=APP_NAME?> LDAP Manager<?=$title?></title>
