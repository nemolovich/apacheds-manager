<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>

    <div class = "container">
      <div class="wrapper">
        <form action="<?=SERVER_ROOT.$_SERVER['PHP_SELF']?>" method="post" name="login" class="form-signin">

          <?php
            if ($_SESSION['redirect_url']) {
              $redirect_url = SERVER_ROOT.$_SESSION['redirect_url'];
            } else {
              $redirect_url = SERVER_ROOT.'/';
            }
            if (isset($_POST['username'])) {
              $connected = connect($_POST['username'],$_POST['password']);
              
              if (!$connected) {
                $message[] = "Connection error";
              }
              $_SESSION['message'] = $message;
              if ($_SESSION['ldap_user'][0]['initials'][0] == 'new') {
                $message[] = "Welcome <strong>".$_SESSION['user']."</strong>";
                $message[] = "Please update your account informations";
                $_SESSION['message'] = $message;
                $_SESSION['message_type'] = "info";
                echo "<meta http-equiv='refresh' content='0; url=".SERVER_ROOT."/user/profile.php' >";
              } else {
                echo "<meta http-equiv='refresh' content='0; url=$redirect_url' >";
              }
            }
          ?>

          <?php if (isset($_SESSION["user"])) : ?>
            <h3 class="form-signin-heading">Welcome</h3>
            <div>
              You are connected on your <?=APP_NAME?> LDAP.
            </div>

            <hr class="colorgraph"><br>

            <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
            
            Your are logged as <b><?=$_SESSION["user"]?></b>
            <a class="top-icon tools" href="<?=SERVER_ROOT?>/user/profile.php" title="Edit my profile"></a>
          <?php else : ?>
            <h3 class="form-signin-heading">Connection</h3>
            <div>
              You can connect with your <?=APP_NAME?> LDAP user/password.
            </div>

            <hr class="colorgraph"><br>

            <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>

            <input type="text" class="form-control" name="username" placeholder="LDAP User Login or Email" required="" autofocus="" />
            <input type="password" class="form-control" name="password" placeholder="Password" required=""/>

            <button class="btn btn-lg btn-primary btn-block" name="Submit" value="Login" type="submit">Login</button>
          <?php endif; ?>
        </form>
      </div>
    </div>
  </body>
</html>
