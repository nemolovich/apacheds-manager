var DEFAULT_USER_ICON;

function uploadFile(input, callback) {
  var file = input.files[0];
  if (file) {
    var reader = new FileReader();
    // reader.readAsText(file, "UTF-8");
    reader.onload = function (evt) {
      var newImage = document.createElement('img');
      newImage.src = evt.target.result;
      newImage.width = 256;
      newImage.height = 256;
      var fileContent = document.getElementById("file-content");
      var imgText = newImage.outerHTML;
      fileContent.innerHTML = imgText;

      var dataEncoded = imgText.replace(/ (?:width|height)="\d+"/g, '').replace(/^<img src="data:image\/(png|jpeg);base64,/, '').replace(/">$/, '');
      var type = imgText.replace(/;base64,.+">$/, '').replace(/^<img src="data:image\//, '');
      
      callback(dataEncoded, type);
    }
    reader.readAsDataURL(file);
  }
  return false;
}

function updateImageContent(data, type) {
  document.getElementById('remove-img').value = "false";
  document.getElementById('avatar-img').src = 'data:image/' + type + ';base64,' + data;
}

function removeAvatar() {
  document.getElementById('remove-img').value = "true";
  document.getElementById('avatar-img').src = DEFAULT_USER_ICON;
}
