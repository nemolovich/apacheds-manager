function add_required_on_labels() {
  $('input[required],select[required]').each((i,e) => {
    const label = $('label[for="'+e.name+'"]')[0];
    label.classList.add('required');
  });
}

$(document).ready(add_required_on_labels);
