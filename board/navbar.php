  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
    
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=SERVER_ROOT?>/">LDAP <?=APP_NAME?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-collapse-3">
      <ul class="nav navbar-nav navbar-right">
      <?php
        $connected = isset($_SESSION["user"]) || NO_GROUPS;
        $isAdmin = $connected && is_admin($_SESSION["user_dn"]);
        $userName = $_SESSION["user"];
        $userIcon = null;
        if ($connected) {
          $is_admin = is_admin($_SESSION['user_dn']);
          if (isset($_SESSION['img'])) {
            $img64 = $_SESSION['img'];
            $userIcon="data:image/png;base64,".base64_encode($img64);
          } else {
            $userIcon = DEFAULT_USER_ICON;
          }
        }
        
        function getDynamicValue($variable) {
          global $connected;
          global $isAdmin;
          global $userName;
          global $userIcon;
          $res = $variable;
          if (preg_match('/^\\$\w+$/', $variable)) {
            $var = preg_replace("/^\\$/", "", $variable);
            if (${$var} != null) {
              $res = ${$var};
            } else {
              $_SESSION['message_type'] = "warning";
              $_SESSION['message'][] = "Variable '$variable' not found for sitemap";
            }
          }
          return $res;
        }
        
        function createNavLink($navLink) {
          global $connected;
          global $isAdmin;
          global $userName;
          global $userIcon;
          $linkType = $navLink->type;
          $linkConfig = $navLink->config;
          $linkCondition = $linkConfig->condition;
          $well_conditioned = preg_match('/^(?:not )?\w+$/', $linkCondition);
          $display = true;
          if ($well_conditioned) {
            $neg_condition = preg_match("/^not \w+$/", $linkCondition);
            $condition = preg_replace("/not /", "", $linkCondition);

            $display = $neg_condition && !(${$condition}) || !$neg_condition && (${$condition});
          } else if ($linkCondition != null) {
            $_SESSION['message_type'] = "warning";
            $_SESSION['message'][] = "Condition for sitemap object '$linkText' is invalid";
          }
          if ($display) {
            $linkText = getDynamicValue($navLink->text);
            $liClass = "";
            $icon = "";
            if (null != $linkConfig->topicon) {
              $liClass = "top-icon ".$linkConfig->topicon;
            } else if (null != $linkConfig->icon) {
              $iconConf = $linkConfig->icon;
              $iconClass = $iconConf->className;
              $iconSrc = getDynamicValue($iconConf->src);
              if (null != $iconSrc) {
                $icon = "<img class=\"$iconClass\" src=\"$iconSrc\">";
              }
            }
            if (null != $linkConfig->className) {
              $liClass = "$liClass ".$linkConfig->className;
            }
            if ($linkType == 'link') {
              $relativePath = $linkConfig->relative;
              $linkHref = $linkConfig->href;
              if ($relativePath) {
                $linkHref = SERVER_ROOT.$linkHref;
              }
              $linkRedirect = $linkConfig->redirect === true ? 'target="_blank"' : '';
              echo "<li><a class=\"$liClass\" $linkRedirect href=\"$linkHref\">$linkText</a></li>";
            } else if ($linkType == 'dropdown') {
              $links = $linkConfig->links;
              echo "<li><a class=\"$liClass dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">".$icon.$linkText."</a>";
              echo "<ul class=\"dropdown-menu\">";
              foreach ($links as $link) {
                createNavLink($link);
              }
              echo "</ul></li>";
            } else if ($linkType == 'divider') {
              echo "<li class=\"divider\"></li>";
            } else if ($linkType == 'custom') {
              if (count(unserialize(CUSTOM_SITEMAP)) > 0) {
                foreach (unserialize(CUSTOM_SITEMAP) as $customLink) {
                  createNavLink($customLink);
                }
              }
            }
          }
        }
        
        foreach (unserialize(DEFAULT_SITEMAP) as $navLink) {
          createNavLink($navLink);
        }
      ?>

      </ul>
    </div><!-- /.navbar-collapse -->
    
    </div><!-- /.container -->
  </nav><!-- /.navbar -->

  <script type="text/javascript">
  DEFAULT_USER_ICON='<?=DEFAULT_USER_ICON?>';
  if (!<?=$destroyed?'true':'false'?>) {
    console.log('Session timer enabled');
    setTimeout(function(){
      console.log('Session timout');
      window.location.href='<?=SERVER_ROOT?>/user/disconnect.php';
    }, <?=(SESSION_TIMEOUT+5)*1000?>);
  }
  </script>