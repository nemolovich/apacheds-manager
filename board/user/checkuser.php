<?php
  if (!$_SESSION['user_dn'] || !is_in_managed_group($_SESSION['user_dn'])) {
    $message[] = "You have to be logged to access to this page.";
    $_SESSION['message_type'] = "danger";
    $_SESSION['message'] = $message;
    $_SESSION['redirect_url'] = $_SERVER['REQUEST_URI'];
    echo "<meta http-equiv='refresh' content='0; url=".LOGIN_PAGE."' >";
    exit(1);
  }
?>
