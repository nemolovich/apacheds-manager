<?php
  session_start();
  include $_SERVER['DOCUMENT_ROOT']."/functions.php";
  if (isset($_SESSION['user'])) {
    session_destroy();
    session_unset();
    session_start();
    session_regenerate_id(true);
    $msg[] = "Successfully disconnected.";
    $msg_type = "success";
  } else if ($destroyed) {
    $msg[] = "Session timeout.";
    $msg_type = "warning";
  } else {
    $msg[] = "Disconnect error.";
    $msg_type = "danger";
  }
  $_SESSION['message_type'] = $msg_type;
  $_SESSION['message'] = $msg;
  header("Refresh:0; url=".LOGIN_PAGE);
?>
