<script type="text/javascript">
  function loadDetails(cn, description, isAdmin, isDefault, users) {
    $('#group-header').html('Details about <b>' + cn + '</b>');
    $('#group-cn').html(cn);
    $('#group-description').html(description);
    $('#group-admin').html(isAdmin ? 'Yes' : 'No');
    $('#group-default').html(isDefault ? 'Yes' : 'No');
    $('#group-users').html('<ul>' + users.split(',').map((u) => '<li>' + u + '</li>').join('\n') + '</ul>');
    return false;
  }
</script>

<div class="modal fade group-modal" tabindex="-1" role="dialog" aria-labelledby="group-details" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="group-header" class="modal-title" id="gridModalLabel">Group Details</h4>
      </div>
      <div class="modal-body">
        <div class="group-modal-content">
          <div class="group-details-block">
            <div class="group-block">
              <span class="span-label">Group CN</span>
              <span class="span-value" id="group-cn">
                User UID
              </span>
            </div>
            <div class="group-block">
              <span class="span-label">Description</span>
              <span class="span-value" id="group-description">
                Group Description
              </span>
            </div>
            <div class="group-block">
              <span class="span-label">Admin group</span>
              <span class="span-value" id="group-admin">
                No
              </span>
            </div>
            <div class="group-block">
              <span class="span-label">Default Group</span>
              <span class="span-value" id="group-default">
                No
              </span>
            </div>
            <div class="group-block">
              <span class="span-label">Users</span>
              <span class="span-value" id="group-users">
                Group Users
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
