<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Groups"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
    <script src="<?=SERVER_ROOT?>/js/jquery-ui.1.12.0.min.js"></script>
    <script>
      function deleteGroup(groupCn) {
        if (confirm(`Delete Group [${groupCn}]?`)) {
          const f = document.createElement('form');
          f.action='<?=SERVER_ROOT?>/admin/delgroup.php?from=<?=$_SERVER['PHP_SELF']?>';
          f.method='POST';
          const i=document.createElement('input');
          i.type='hidden';
          i.name='group_cn';
          i.value=groupCn;
          f.appendChild(i);

          document.body.appendChild(f);
          f.submit();
        }
      }
    </script>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/user/groupsdetails.php"; ?>

    <div class = "container">
      <div class="wrapper">
        <div method="post" name="listUsers" class="form-signin form-large">
          <h3 class="form-signin-heading">Groups list</h3>
          <hr class="colorgraph"><br>

          <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
          <?php include $_SERVER['DOCUMENT_ROOT']."/user/checkuser.php"; ?>

          <div class="table-responsive">
            <table class="group-list table table-hover table-bordered table-striped">
              <thead class="thead-inverse">
                <tr>
                  <th>CN</th>
                  <th>Description</th>
                  <th>Admin</th>
                  <th>Default</th>
                  <th>Members</th>
                  <th>Actions</th>
                </tr>
              <thead>
              <tbody>
                <?php
                  $isAdmin = is_admin($_SESSION['user_dn']);
                ?>
                <?php foreach(getGroupsWithUsers() as $group_array): ?>
                  <?php if (is_managed_group($group_array['cn'][0])) : ?>
                  <?php
                    $group_users = $group_array['users'];
                    $user_details = function($user) {
                      return $user['name']." (".$user['uid'].")";
                    };
                    $admin_group = in_array($group_array['cn'][0], ADMINS_GROUPS);
                    $default_group = $group_array['cn'][0] == DEFAULT_GROUP;
                  ?>
                  <tr>
                    <td>
                      <?=$group_array['cn'][0]?>
                    </td>
                    <td>
                      <?=$group_array['description'][0]?>
                    </td>
                    <td style="text-align:center;">
                      <?=$admin_group ? '<span class="checkmark" title="The members of this group have admin rights"></span>' : ''?>
                    </td>
                    <td style="text-align:center;">
                      <?=$default_group ? '<span class="checkmark" title="This group is the default group"></span>' :  ''?>
                    </td>
                    <td style="text-align:center;">
                      <?=count($group_users)?>
                    </td>
                    <td>
                      <form method="post">
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target=".group-modal"
                            onclick="loadDetails('<?=$group_array['cn'][0]?>',
                                       '<?=$group_array['description'][0]?>',
                                       '<?=$admin_group?>',
                                       '<?=$default_group?>',
                                       '<?=implode(', ', array_map($user_details, $group_users))?>');return false;">Details</button>
                        <?php if ($isAdmin) : ?>
                          <button class="btn btn-warning btn-sm" 
                              onclick="window.location='<?=SERVER_ROOT?>/admin/editgroup.php?cn=<?=$group_array['cn'][0]?>&from=<?=$_SERVER['PHP_SELF']?>';return false;">Update</button>
                          <?php
                            $isDefault = $group_array['cn'][0] == DEFAULT_GROUP;
                            $isLastAdmin = count(ADMINS_GROUPS) < 2 && in_array($group_array['cn'][0], ADMINS_GROUPS);
                            $disabled = $isDefault || $isLastAdmin;
                          ?>
                          <button class="btn btn-danger btn-sm <?=$disabled ? 'disabled' : ''?>" 
                              <?=$disabled ? 'disabled="disabled" title="'.($isDefault ? "This is the default group" : "This is the only admins group").'"' : 'onclick="deleteGroup(\''.$group_array['cn'][0].'\');return false;"'?>>Delete</button>
                        <?php endif; ?>
                      </form>
                    </td>
                  </tr>
                <?php endif; ?>
                <?php endforeach; ?>
              </tbody>
            </table>

            <button class="btn btn-success btn-sm" 
                onclick="window.location='<?=SERVER_ROOT?>/admin/addgroup.php?from=<?=$_SERVER['PHP_SELF']?>';return false;">Create group</button>
          </div>
        </form>
      </div>
    </div>


  </body>
</html>
