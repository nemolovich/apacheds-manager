<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Profile"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
    <!-- Custom JS script -->
    <script src="<?=SERVER_ROOT?>/js/form.js"></script>
    <script src="<?=SERVER_ROOT?>/js/fileupload.js"></script>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>

    <div class = "container">
      <div class="wrapper">
        <form action="<?=SERVER_ROOT.$_SERVER['PHP_SELF']?>" method="post" id="updateProfile"
            name="updateProfile" class="form-signin" enctype="multipart/form-data">
          <h3>Edit your profile</h3>
          <h4>User: <?=$_SESSION['user']?> [<?=$_SESSION['user_id']?>]</h4>

          <hr class="colorgraph"><br>

          <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
          <?php include $_SERVER['DOCUMENT_ROOT']."/user/checkuser.php"; ?>

          <?php
            /* Check if form has been submit */
            if (isset($_POST['firstname'])) {
              global $message;
              $defaultImg = $_SESSION['img'];
              $imgContent = getFormImgContent($defaultImg);
              $udpated = updateMyProfile($_SESSION['user_id'], $_POST['firstname'], $_POST['name'],
                     $_POST['mail'], $_POST['officePhone'], $_POST['mobile'],
                     $_POST['jobTitle'], $imgContent, $_POST['password'],
                     $_POST['passwordCheck']);
              if ($udpated) {
                $_SESSION['message_type'] = "success";
              } else {
                $message[] = "Modification error";
                $_SESSION['message_type'] = "danger";
              }
              $_SESSION['message'] = $message;
              echo "<meta http-equiv='refresh' content='0'>";
            }
          ?>
          <?php
            $user_ldap = $_SESSION['ldap_user'][0];
            $phonePattern = "^(02(-| )?49(-| )?53)((-| )?(\d{2})){2}";
            $mobilePattern = "^(0[67])((-| )?(\d{2})){4}";
            if (isset($_SESSION['img'])) {
              $img64 = $_SESSION['img'];
              $src="data:image/png;base64,".base64_encode($img64);
              $remove = "false";
            } else {
              $src = DEFAULT_USER_ICON;
              $remove = "true";
            }
          ?>
            <input type="hidden" name="userid" value="<?=$_SESSION['user_id']?>" />
            <label for="firstname">Firstname: </label>
            <input type="hidden" id="remove-img" name="remove-img" value="<?=$remove?>" />
            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" value="<?=$user_ldap['cn'][0]?>" required="" autofocus="" />
            <label for="name">Name: </label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?=$user_ldap['sn'][0]?>" required="" />
            <label for="mail">Email: </label>
            <input type="email" class="form-control" id="mail" name="mail" placeholder="Email" value="<?=$user_ldap['mail'][0]?>" required="" />
            <label for="officePhone">Office phone number: </label><br/>
            <span class="help-text">Office number must be like 02 49 53 XX XX</span>
            <input type="tel" class="form-control" id="officePhone" name="officePhone" placeholder="Office phone number" value="<?=$user_ldap['telephonenumber'][0]?>" pattern="<?=$phonePattern?>" />
            <label for="mobile">Mobile number: </label><br/>
            <span class="help-text">Personal phone number 06/07 XX XX XX XX</span>
            <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="Mobile number" value="<?=$user_ldap['mobile'][0]?>" pattern="<?=$mobilePattern?>" />
            <label for="jobTitle">Job Title: </label><br/>
            <input type="text" class="form-control" id="jobTitle" name="jobTitle" placeholder="Job Title" value="<?=$user_ldap['title'][0]?>" />

            <label for="avatar-img">Edit avatar: </label>
            <img class="form-control user-icon upload-img" id="avatar-img" src="<?=$src?>" onclick="document.getElementById('avatar').click();return false;" />

            <div class="file-upload btn btn-default btn-sm">
              <span>Upload an image</span>
              <input type="file" id="avatar" name="avatar"
                   onchange="uploadFile(this, updateImageContent);"
                   class="upload" />
            </div>
            <button class="file-upload btn btn-default btn-sm" type="button" id="remove" name="remove" onclick="removeAvatar();">Remove Avatar</button>
            <br/>

            <label for="password">Password: </label>
            <span class="help-popup label label-info">Info
              <div>
                <ul>
                  <li>The password must have at least 8 characters</li>
                  <li>It must be composed by at least one upper character, lower character and number</li>
                </ul>
              </div>
            </span>
            <span class="icon-bar"></span>
            <br/>
            <span class="help-text">Leave empty for no update</span>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
            <label for="passwordCheck">Verification: </label>
            <input type="password" class="form-control" id="passwordCheck" name="passwordCheck" placeholder="Pasword verification" />

          <span class="help-text required">= Required fields</span>
          <button class="btn btn-lg btn-primary btn-block" name="Submit" value="Save" type="submit">Save</button>
          <button class="btn btn-block btn-default" name="Cancel" value="Cancel" type="button" onclick="window.location='<?=SERVER_ROOT?>/';return false;">Cancel</button>
        </form>
        <div id="file-content" class="file-content">
        </div>
      </div>
    </div>
  </body>
</html>
