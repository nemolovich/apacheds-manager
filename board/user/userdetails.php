<script type="text/javascript">
  function loadDetails(uid, firstname, name, mail, phone, mobile, title, img, groups) {
    $('#user-header').html('Details about <b>' + firstname + ' ' + name + '</b>');
    $('#user-img').html('<img src="' + img + '">');
    $('#user-uid').html(uid);
    $('#user-firstname').html(firstname);
    $('#user-name').html(name);
    $('#user-mail').html('<a href="mailto:' + mail + '"\
                 title="Send a mail to ' + firstname + ' ' + name + '">\
                 ' + mail + '\
                </a><span>&nbsp;</span><a class="glyphicon glyphicon-edit pull-right" target="_blank" href="'+ '<?=CHAT_LINK?>'.replace(/\{%mail%\}/, mail) + '"\
                 title="Chat with ' + firstname + ' ' + name + '"></a>');
    $('#user-phone').html('<a href="tel:' + phone.replace(/ /g, '') + '"\
                 title="Call ' + phone + '">\
                 ' + phone + '\
                </a>');
    $('#user-mobile').html('<a href="tel:' + mobile.replace(/ /g, '') + '"\
                 title="Call ' + mobile + '">\
                 ' + mobile + '\
                </a>');
    $('#user-title').html(title);
    $('#user-group').html(groups);
    return false;
  }
</script>

<div class="modal fade user-modal" tabindex="-1" role="dialog" aria-labelledby="user-details" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="user-header" class="modal-title" id="gridModalLabel">User Details</h4>
      </div>
      <div class="modal-body">
        <div class="user-modal-content">
          <div class="user-img" id="user-img">
            <img src="<?=DEFAULT_USER_ICON?>">
          </div>
          <div class="user-details-block">
            <div class="user-block">
              <span class="span-label">User UID</span>
              <span class="span-value" id="user-uid">
                User UID
              </span>
            </div>
            <div class="user-block">
              <span class="span-label">First Name</span>
              <span class="span-value" id="user-firstname">
                User Firstname
              </span>
            </div>
            <div class="user-block">
              <span class="span-label">Last Name</span>
              <span class="span-value" id="user-name">
                User Name
              </span>
            </div>
            <div class="user-block">
              <span class="span-label">Email</span>
              <span class="span-value" id="user-mail">
                User Mail
              </span>
            </div>
            <div class="user-block">
              <span class="span-label">Phone Number</span>
              <span class="span-value" id="user-phone">
                User Phone Number
              </span>
            </div>
            <?php if(is_admin($_SESSION['user_dn'])): ?>
              <div class="user-block">
                <span class="span-label">Mobile Number</span>
                <span class="span-value" id="user-mobile">
                  User Mobile Number
                </span>
              </div>
            <?php endif; ?>
            <div class="user-block">
              <span class="span-label">Job Title</span>
              <span class="span-value" id="user-title">
                User Job Title
              </span>
            </div>
            <div class="user-block">
              <span class="span-label">Groups</span>
              <span class="span-value" id="user-group">
                User Groups
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
