<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php $title=" | Users"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/header.php"; ?>
    <script src="<?=SERVER_ROOT?>/js/jquery-ui.1.12.0.min.js"></script>
    <script>
      function deleteUser(userId) {
        if (confirm(`Delete User [${userId}]?`)) {
          const f = document.createElement('form');
          f.action='<?=SERVER_ROOT?>/admin/deluser.php';
          f.method='POST';
          const i=document.createElement('input');
          i.type='hidden';
          i.name='user_id';
          i.value=userId;
          f.appendChild(i);

          document.body.appendChild(f);
          f.submit();
        }
      }
    </script>
  </head>
  <body>

    <?php include $_SERVER['DOCUMENT_ROOT']."/navbar.php"; ?>
    <?php include $_SERVER['DOCUMENT_ROOT']."/user/userdetails.php"; ?>

    <div class = "container">
      <div class="wrapper">
        <div method="post" name="listUsers" class="form-signin form-large">
          <h3 class="form-signin-heading">Users list</h3>
          <hr class="colorgraph"><br>

          <?php include $_SERVER['DOCUMENT_ROOT']."/checkmessages.php"; ?>
          <?php include $_SERVER['DOCUMENT_ROOT']."/user/checkuser.php"; ?>

          <div class="table-responsive">
            <table class="users-list table table-hover table-bordered table-striped">
              <thead class="thead-inverse">
                <tr>
                  <th>UID</th>
                  <th>First&nbspName</th>
                  <th>Last&nbspName</th>
                  <th>Email</th>
                  <th>Actions</th>
                </tr>
              <thead>
              <tbody>
                
                <?php
                  $isAdmin = is_admin($_SESSION['user_dn']);
                ?>
                <?php foreach(getUsersWithGroup() as $user_array): ?>
                  <?php
                    $user_groups = $user_array['groups_cn'];
                    if (isset($user_array['jpegphoto'][0])) {
                      $img64 = $user_array['jpegphoto'][0];
                      $src="data:image/png;base64,".base64_encode($img64);
                    } else {
                      $src = DEFAULT_USER_ICON;
                    }
                  ?>
                  <?php if(is_in_managed_group($user_array['user_dn'])) : ?>
                    <tr>
                      <td>
                        <div class="user-img uuid-icon">
                          <img src="<?=$src?>">
                        </div>
                        <?=$user_array['uid'][0]?>
                      </td>
                      <td>
                        <?=$user_array['cn'][0]?>
                      </td>
                      <td>
                        <?=$user_array['sn'][0]?>
                      </td>
                      <td>
                        <a href="mailto:<?=$user_array['mail'][0]?>"
                           title="Send a mail to <?=$user_array['displayname'][0]?>">
                           <?=$user_array['mail'][0]?>
                        </a>
                        <a class="glyphicon glyphicon-edit pull-right" target="_blank" href="<?=getChatLink($user_array['mail'][0])?>"
                           title="Chat with <?=$user_array['displayname'][0]?>">
                        </a>
                      </td>
                      <td>
                        <form method="post">
                          <button class="btn btn-info btn-sm" data-toggle="modal" data-target=".user-modal"
                              onclick="loadDetails('<?=$user_array['uid'][0]?>',
                                         '<?=$user_array['cn'][0]?>',
                                         '<?=$user_array['sn'][0]?>',
                                         '<?=$user_array['mail'][0]?>',
                                         '<?=$user_array['telephonenumber'][0]?>',
                                         '<?=$user_array['mobile'][0]?>',
                                         '<?=$user_array['title'][0]?>',
                                         '<?=$src?>',
                                         '<?=implode(', ', $user_groups)?>');return false;">Details</button>
                          <?php if ($isAdmin) : ?>
                            <?php
                              if ($user_array['uid'][0] == $_SESSION['user_id']) {
                                $editUrl = SERVER_ROOT."/user/profile.php";
                              } else {
                                $editUrl = SERVER_ROOT."/admin/edituser.php?uid=".$user_array['uid'][0].'&from='.$_SERVER['PHP_SELF'];
                              }
                            ?>
                            <button class="btn btn-warning btn-sm" 
                                onclick="window.location='<?=$editUrl?>';return false;">Update</button>
                            <?php
                              $disabled = $user_array['uid'][0] == $_SESSION['user_id'];
                            ?>
                            <button class="btn btn-danger btn-sm <?=$disabled ? 'disabled' :''?>" 
                                <?=$disabled ? 'disabled="disabled"' : 'onclick="deleteUser(\''.$user_array['uid'][0].'\');return false;"'?>>Delete</button>
                          <?php endif; ?>
                        </form>
                      </td>
                    </tr>
                  <?php endif; ?>
                <?php endforeach; ?>
              </tbody>
            </table>

            <button class="btn btn-success btn-sm" 
                onclick="window.location='<?=SERVER_ROOT?>/admin/adduser.php?from=<?=$_SERVER['PHP_SELF']?>';return false;">Create user</button>
          </div>
        </form>
      </div>
    </div>


  </body>
</html>
